from django.urls import re_path,path
from . import views
from . import consumers

websocket_urlpatterns = [
    path('', views.index, name='index'),
    re_path(r'ws/chat/(?P<room_name>\w+)/$', consumers.ChatConsumer),
]