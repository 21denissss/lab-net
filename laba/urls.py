from django.conf.urls import url
from django.urls import path, include
from django.contrib.auth.views import *
from laba import views

urlpatterns = [
    path('', views.index, name='index_main'),
    path('accounts/login/', LoginView.as_view(), name='login'),
    path('accounts/logout/', LogoutView.as_view(), name='logout'),
    path('file/add/', views.FileCreateView.as_view(), name='file_add'),
    path('files/', views.FileListView.as_view(), name='file_list'),
    path('files/<int:pk>', views.FileDetailView.as_view(), name='file_detail'),
    path('files/<int:pk>/download', views.download_file, name='file_download'),
    path('messages/', views.MessageListView.as_view(), name='messages_list'),
    path('post/list/', views.PostListView.as_view(),name='post_list'),
    path('post/create/', views.PostCreateView.as_view(),name='post_create'),
    path('post/<int:pk>/', views.PostDetailView.as_view(),name='post_detail'),
]