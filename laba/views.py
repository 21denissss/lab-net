from django.urls import reverse_lazy
from django.views.generic import *
from django.shortcuts import render, redirect
from django.views.generic.edit import FormMixin
from django.db.models.signals import pre_save
from django.dispatch import receiver
from laba.forms import MessageForm
from laba.models import *
from django.contrib.auth.models import User
import os
from django.conf import settings
from django.http import HttpResponse, Http404, FileResponse


# Create your views here.
def index(request):
    return render(request, 'index.html', {})


@receiver(pre_save, sender=Message)
def my_handler(sender, **kwargs):
    print(User.objects.all())
    return redirect('file_list')


def download_file(request, pk):
    file = FileModel.objects.get(pk=pk)
    filename = file.file.path
    response = FileResponse(open(filename, 'rb'))
    return response


class FileCreateView(CreateView):
    model = FileModel
    fields = ['name', 'file']

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if (form.is_valid()):
            file = form.save(commit=False)
            file.author = request.user
            file.date = timezone.now()
            file.file = request.FILES['file']
            file.save()
        return redirect('file_list')


class FileListView(ListView):
    model = FileModel
    context_object_name = 'files'
    template_name = "laba/files_list.html"


class FileDetailView(DetailView):
    model = FileModel
    context_object_name = 'file'
    template_name = 'laba/file_detail.html'


class MessageListView(FormMixin, ListView):
    model = Message
    context_object_name = 'messages'
    template_name = "laba/messages_list.html"
    form_class = MessageForm

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(MessageListView, self).get_context_data(**kwargs)
        form = MessageForm()
        context.update({'form': form})
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if (form.is_valid()):
            message = form.save(commit=False)
            message.author = request.user
            message.date = timezone.now()
            message.save()
        return redirect('messages_list')


class PostListView(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = "laba/posts_list.html"


class PostCreateView(CreateView):
    model = Post
    fields = ['title', 'text']

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if (form.is_valid()):
            post = form.save(commit=False)
            post.author = request.user
            post.date = timezone.now()
            post.save()
        return redirect('post_list')


class PostDetailView(DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'laba/post_detail.html'
